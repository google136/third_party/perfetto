# This file has been generated by: tools/roll-prebuilts v29.0
TRACE_PROCESSOR_SHELL_MANIFEST = [{
    'arch':
        'mac-amd64',
    'file_name':
        'trace_processor_shell',
    'file_size':
        8335984,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/mac-amd64/trace_processor_shell',
    'sha256':
        '10dda4be4dfbbd3249513c5c9e07bc49bedb842461fbbde6b913e1413c1a9f2b',
    'platform':
        'darwin',
    'machine': ['x86_64']
}, {
    'arch':
        'mac-arm64',
    'file_name':
        'trace_processor_shell',
    'file_size':
        7088104,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/mac-arm64/trace_processor_shell',
    'sha256':
        'f97a890390083a2da5d9bbea4490c0710a0096d1c80f29aa46ef9ded452d3423',
    'platform':
        'darwin',
    'machine': ['arm64']
}, {
    'arch':
        'linux-amd64',
    'file_name':
        'trace_processor_shell',
    'file_size':
        9274816,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/linux-amd64/trace_processor_shell',
    'sha256':
        '5a1031d85359f8b6f21d56107c8e4b5548d37820d4155d347785e44ee07d7a83',
    'platform':
        'linux',
    'machine': ['x86_64']
}, {
    'arch':
        'linux-arm',
    'file_name':
        'trace_processor_shell',
    'file_size':
        7032844,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/linux-arm/trace_processor_shell',
    'sha256':
        '44028e76b37a106b8f22648c126b31cb7c7539932a98619fef43f2ab177dd1f2',
    'platform':
        'linux',
    'machine': ['armv6l', 'armv7l', 'armv8l']
}, {
    'arch':
        'linux-arm64',
    'file_name':
        'trace_processor_shell',
    'file_size':
        8393040,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/linux-arm64/trace_processor_shell',
    'sha256':
        'b0e4ab09f04f2649543bb8d155279440de2287602ed3d109eca2116dbd16832e',
    'platform':
        'linux',
    'machine': ['aarch64']
}, {
    'arch':
        'android-arm',
    'file_name':
        'trace_processor_shell',
    'file_size':
        5676736,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/android-arm/trace_processor_shell',
    'sha256':
        '627b4ebc9eedd80e923bd1d271a05e42d8025d2764610806a11fcc8953f56e07'
}, {
    'arch':
        'android-arm64',
    'file_name':
        'trace_processor_shell',
    'file_size':
        7291616,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/android-arm64/trace_processor_shell',
    'sha256':
        'a2547c5b59c85181d005af2a560f7a6c72224a96eee580325031a5525ac9d8cb'
}, {
    'arch':
        'android-x86',
    'file_name':
        'trace_processor_shell',
    'file_size':
        8208020,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/android-x86/trace_processor_shell',
    'sha256':
        'e8411bbd56f04f0996117f984e40a60366d14aac712448555a44df56cba6f3da'
}, {
    'arch':
        'android-x64',
    'file_name':
        'trace_processor_shell',
    'file_size':
        8553248,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/android-x64/trace_processor_shell',
    'sha256':
        '8779baed1ddc7a5e364f838a1587bbf265157ddb38db7c0d1a07f82f7106bf19'
}, {
    'arch':
        'windows-amd64',
    'file_name':
        'trace_processor_shell.exe',
    'file_size':
        8883712,
    'url':
        'https://commondatastorage.googleapis.com/perfetto-luci-artifacts/v29.0/windows-amd64/trace_processor_shell.exe',
    'sha256':
        '444804dfede101b8cdc07812ccec54f8b433309fc1586bb46db0f7cf6f49a761',
    'platform':
        'win32',
    'machine': ['amd64']
}]
